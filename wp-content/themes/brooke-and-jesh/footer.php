<!--
	<footer>
		<div class="row footer-wrap">
			<div class="small-12 columns text-center">	
				<p>Any questions? <a href="mailto:jeshua.sharkey@gmail.com">jeshua.sharkey@gmail.com</a></p>
			</div>
		</div>
	<footer>
-->
	<?php wp_footer(); ?>
	
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-92528005-1', 'auto');
		ga('send', 'pageview');
	</script>
	
    <script>
      jQuery(document).foundation();
    </script>
</body>
</html>
