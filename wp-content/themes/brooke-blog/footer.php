				<footer class="footer" role="contentinfo">
					© Brooke & Jesh <a href="mailto:brookeandjesh@gmail.com">Email Us</a>
				</footer> <!-- end .footer -->
			</div>  <!-- end .main-content -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->