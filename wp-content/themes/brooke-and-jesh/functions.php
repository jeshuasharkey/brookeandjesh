<?php

add_theme_support( 'menus' ); 
add_theme_support( 'post-thumbnails' ); 


// Enqueue theme scripts and styles
function foundation_parent_scripts(){
	// Scripts
    wp_enqueue_script( 'commonJS', get_template_directory_uri() . '/js/common-parent.js' , array( 'jquery' ) );
    wp_enqueue_script( 'foundationJS', get_template_directory_uri() . '/js/foundation.min.js' , array( 'jquery' ) );
    wp_enqueue_script( 'svgInliner', get_template_directory_uri() . '/js/svg-inliner.js' , array( 'jquery' ) );
    wp_enqueue_script( 'cookieJS', get_template_directory_uri() . '/js/js.cookie.js' , array( 'jquery' ) );
    // Styles
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/styles/normalize.css' );
    wp_enqueue_style( 'foundationCSS', get_template_directory_uri() . '/styles/foundation.css' );
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'fonts', get_template_directory_uri() . '/styles/fonts/fonts.css' );
    wp_enqueue_style( 'responsive', get_template_directory_uri() . '/styles/responsive.css' );
    
    wp_localize_script('commonJS', 'WPURLS', array( 'siteurl' => get_option('siteurl') ));
}

add_action( 'wp_enqueue_scripts', 'foundation_parent_scripts' );

// Enqueue login scripts and styles
function login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/styles/login.css' );
    echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
}
add_action( 'login_enqueue_scripts', 'login_stylesheet' );

function update_quantity() {
  $curr_quantity = $_POST['quantity'];
  $new_quantity = --$curr_quantity;
  
  $post_id = $_POST['postID'];
  
  echo $new_quantity;
  
  update_field('quantity_requested', $new_quantity, $post_id);
  
  die();
}
add_action( 'wp_ajax_nopriv_update_quantity',  'update_quantity' );
add_action( 'wp_ajax_update_quantity','update_quantity' );


?>