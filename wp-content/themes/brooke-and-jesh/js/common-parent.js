jQuery(document).ready(function($) {
	
	new SVGInliner(document.querySelectorAll(".svg"));
	
	$('.cool-open').on('click', function(){
		$('.cool-things-to-do').toggleClass('active');
	});
	
	$('.product-wrap').on('click', function(){
		var modalContent = $(this).children('.modal-html-content').html();
		var modalOverlay = $(this).children('.modal-buy-overlay').html();
		$('.product-modal .modal-html-content').html(modalContent);
		$('.product-modal .modal-buy-overlay').html(modalOverlay);
		$('.product-modal-overlay').addClass('active');
		$("body").addClass("modal-open");
	});
	
	$('.modal-close-button').on('click', function(){
		$('.product-modal-overlay').removeClass('active');
		$("body").removeClass("modal-open");
	});
	
	$(document).on('click', 'a.anchor', function(event){
	    event.preventDefault();
	
	    $('html, body').animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 500);
	});
	
	$('a.registry-buy').live('click', function(e){
		var productID = $(this).data('productid');
		var cookieName = 'product'+productID;
		if(Cookies.get(cookieName) != 1){
			$('.product-modal .modal-buy-overlay').addClass('active');
		} else if(Cookies.get(cookieName) == 1 && !$(this).hasClass('continue')){
			e.preventDefault();
			$(this).before('<p class="continue-alert"><i>It looks like you already bought this, do you want to buy it again?</i></p>');
			$(this).html('Continue');
			$(this).addClass('continue');
			Cookies.remove(cookieName);
		}
	});
	
	$('.product-modal .modal-buy-overlay .button-wrap .button').live('click', function(){
		$('.product-modal .modal-buy-overlay').removeClass('active');
	});
	
	$('.product-modal .modal-buy-overlay .button-wrap .button.confirm').live('click', function(){
		
		$('.product-modal .continue-alert').remove();
		$('a.registry-buy').html('Buy Online');
		
		var ajaxurl = WPURLS.siteurl+'/wp-admin/admin-ajax.php';
		var quantity = $('.product-modal .modal-buy-overlay .button-wrap').data("quantity");
		var postID = $('.product-modal .modal-buy-overlay .button-wrap').data("postid");
		
		Cookies.set('product'+postID, 1, { expires: 30 });

		if(quantity > 0){
			$.ajax({
				url: ajaxurl + "?action=update_quantity",
				type: 'post',
				data: { quantity:quantity, postID:postID },
				success: function(data) {
					console.log("SUCCESS! data: "+data);
					$(".product-modal .modal-html-content .quantity-value").html(data);
				},
				error: function(data) {
					console.log("FAILURE");
				}
			});
		}
	});
	
});