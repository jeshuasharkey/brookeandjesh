<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>


	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_title(); ?>
		<?php the_content(); ?>
		<?php comments_template(); ?>

	<?php endwhile; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>