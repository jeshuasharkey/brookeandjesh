<?php
/* The template for displaying the homepage */
get_header(); ?>

<div class="flex-wrap">
	<div class="text">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/images/shark-icon@2x.png" alt="shark-icon@2x" width="50" />
		<div class="text-center">Just Married.</div>
	</div>
</div>

<?php get_footer(); ?>