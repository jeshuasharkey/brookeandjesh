<?php
/* The template for displaying the homepage */
get_header(); ?>

	<h1><?php the_title(); ?></h1>
	
	<div class="number"><?php the_field('value'); ?></div>
	<div class="subtract">-1</a>
	<div id="value" value="<?php the_field('value'); ?>">
	</form>

<?php get_footer(); ?>