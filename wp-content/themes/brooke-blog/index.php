<?php get_header(); ?>
	<div class="content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'parts/loop', 'archive' ); ?>
		<?php endwhile; ?>	
			<?php joints_page_navi(); ?>
		<?php else : ?>
			<p>No Posts Found</p>
		<?php endif; ?>
		<?php get_sidebar(); ?>
	</div> <!-- end #content -->
<?php get_footer(); ?>