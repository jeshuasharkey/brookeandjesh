<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section
 *
 */
?><!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
	<meta charset="utf-8">
	<title><?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;
	
		wp_title( '|', true, 'right' );
	
		// Add the blog name.
		bloginfo( 'name' );
	
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
	
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s' ), max( $paged, $page ) );
	
		?></title>
		
	<?php wp_head(); ?> 
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<! -- Scale correctly for mobile viewers -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body><!--

	<header>
		<div class="row header-wrap">
			<div class="header-nav-wrap right">
				<ul class="menu">	
					<li><a href="#rsvp" class="anchor">RSVP</a></li>
					<li><a href="#location" class="anchor">Location</a></li>
					<li><a href="#registry" class="anchor">Registry</a></li>
				</ul>
			</div>
		</div>
	</header>
-->