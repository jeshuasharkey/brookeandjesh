<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'brookeandjesh');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hc7~b5C@msNG]>0W`e3H-3OSWNehq>Zd&cbpHBc#h>5)lcL<YdT<NE4G!};`<{?B');
define('SECURE_AUTH_KEY',  '*;TdsE1d!7(FW@cc^aM&S}`.U5| j1X1@;H6A*-3W>Mx12XOzTQ,M@--)hG-&|FI');
define('LOGGED_IN_KEY',    'beC|-&leY~HM/:(}#>)n,iH?3&vaQ6-h~h;Wq35 cQiXA0*({&5LZI>8,J83!/jo');
define('NONCE_KEY',        'o-<2,8MNAnqwH&1gAje,2W0ByLR[pnL+rPYr_qt:%F^wJds37gC4wKo84NEWkTPa');
define('AUTH_SALT',        '0]*R>ga)C~k&32J;Y70.AttN#OO*Zy:AZrWvJ4;tMR_<di~`-Z6GFj@:TShsZ.8]');
define('SECURE_AUTH_SALT', 'wAU]tn+M=?uU@FA_QGw,{m%q*a7t/j>^NgB3}l!Y:I=0/}EQE,8S`?IM_KM24XwT');
define('LOGGED_IN_SALT',   '+aGcw W-wZIt/|(Y>#5lvr{!`>w~R?(gT7PCSN0L$l c?mJv%%&N4mZAwXz>|-UU');
define('NONCE_SALT',       'co Z`kTE5cW5%e,EZQU&ewp_DM]XMC?VnJ1N@l53F}4YOacL&>V[uxlk)*;taaD ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
