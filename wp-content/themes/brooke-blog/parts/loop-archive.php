<article class="loop-article" id="post-<?php the_ID(); ?>">	
	<div class="image-wrap">
		<a href="<?php the_permalink() ?>">
			<?php the_post_thumbnail('full'); ?>
		</a>
	</div>
	<a class="title" href="<?php the_permalink() ?>">
		<h1><?php the_title(); ?></h1>
	</a>
	<div class="content-wrap">
		<?php the_content(); ?>
	</div>
	 <div class="date-wrap"><?php the_time('F j, Y'); ?></div>
	<!-- <div class="clear"></div>
	<a href="<?php the_permalink() ?>" class="read-more">Read</a>	    						  -->
</article>