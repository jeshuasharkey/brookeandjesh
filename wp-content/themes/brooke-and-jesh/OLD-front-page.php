<?php
/* The template for displaying the homepage */
get_header(); ?>

<section id="intro">	
	<div class="row">
		<div class="small-12 medium-push-6 medium-6 columns intro-text-wrap">
			<h1>Brooke<br/>&<br/>Jesh</h1>
			<h4>6 May 2017</h4>
			<p>We first laid eyes on each other in the church foyer but really it was the magical swipe right that sparked this budding romance. Three years later and we're getting married! We are so excited and cant wait for you to join us to celebrate.</p>
			
			<a href="#rsvp" class="button light anchor">Rsvp Now</a>
		</div>
		<div class="small-12 medium-6 medium-pull-6 columns">
			<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/hero-image@2x.jpg" alt="hero-image@2x"/>
			<div class="animation-constrain">
				<div class="shark-animation-wrap">
					<img class="waves-top svg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/waves-top.svg"/>
					<img class="sharks svg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/shark.svg"/>
					<img class="shark-bg svg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/shark-bg.svg"/>
					<img class="waves-bottom svg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/waves-bottom.svg"/>
					<img class="waves-bottom-bg svg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/waves-bottom-bg.svg"/>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="rsvp">
	<div class="row">
		<div class="small-12 text-center columns">
			<div class="rsvp-wrap">
				<h2>RSVP</h2>
				<?php echo do_shortcode('[contact-form-7 id="5" title="RSVP Form"]'); ?>
			</div>
		</div>
	</div>
</section>

<section id="location">	
	<div class="row">	
		<div class="small-12 medium-6 columns">
			<div class="location-wrap">	
				<h3>Ceremony</h3><h4>2pm</h4>
				<p>St Andrews Anglican Church<br/>
					85 Hamilton Road<br/>
					Cambridge<br/>
					Waikato
				</p>
			</div>
			<div class="location-wrap">	
				<h3>Reception</h3><h4>5pm</h4>
				<p>Cambridge Town Hall<br/>
					Victoria Street<br/>
					Cambridge<br/>
					Waikato<br/>
				</p>
			</div>
			<div class="location-wrap">	
				<h3>AfterParty</h3><h4>9pm</h4>
				<p class="small-text">(Optional)</p>
				<p>811a Bruntwood Road<br/>
					Tamahere<br/>
					Hamilton<br/>
					Waikato<br/>
				</p>
			</div>
		</div>
		<div class="small-12 medium-6 columns map-outer">
			<div class="map-wrap">
				<img class="map svg" src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/map.svg"/>
			</div>
			<div class="button dark round cool-open">
				<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/location-pin.svg"/>
				<div class="text">
					Things to<br/>do around cambridge<br/>
					<span class="button-subtext">Click Here</span>
				</div>
			</div>
		</div>
	</div>
	<div class="row cool-things-to-do">
		<div class="small-12 columns">
			<div class="cool-open cross">+</div>
			<div class="item">	
				<div class="number">1.</div>
				<div class="text">
					<h4><a href="https://www.google.co.nz/maps/place/Paddock/@-37.8951578,175.4681859,17z/data=!3m1!4b1!4m5!3m4!1s0x6d6cfd7cee0e9297:0xd343ecf9fe1b0bdf!8m2!3d-37.8951621!4d175.4703746">Paddock Cafe</a></h4>
					<p>Jesh's favourite coffee stop en route from Auckland to Taupo. <i>46 Victoria St, Cambridge</i>.</p>
				</div>
			</div>
			
			<div class="item">	
				<div class="number">2.</div>
				<div class="text">
					<h4><a href="https://www.google.co.nz/maps/place/Hamilton+Gardens/@-37.8049743,175.3030138,17z/data=!3m1!4b1!4m5!3m4!1s0x6d6d1f297e2c58a3:0xf00ef62249d2b80!8m2!3d-37.8049786!4d175.3052025">Hamilton Gardens</a></h4>
					<p>Sound boring but its actually magical. Seeing is believing.</p>
				</div>
			</div>
			
			<div class="item">	
				<div class="number">3.</div>
				<div class="text">
					<h4><a href="https://www.google.co.nz/maps/place/Winner+Winner+Chicken+Shop/@-37.7919551,175.2904378,17z/data=!3m1!4b1!4m5!3m4!1s0x6d6d18b62b57749f:0xe94c7b7e83cda087!8m2!3d-37.7919594!4d175.2926265">Winner Winner</a></h4>
					<p>A rad new chicken joint for your afternoon cravings. <i>439 Grey St, Hamilton</i>.</p>
				</div>
			</div>
		
			<div class="item">	
				<div class="number">4.</div>
				<div class="text">
					<h4><a href="https://www.google.co.nz/maps/place/Timezone+Victoria+St/@-37.7856129,175.279573,17z/data=!3m1!4b1!4m5!3m4!1s0x6d6d18a4db433073:0xe80753a5b56f9e37!8m2!3d-37.7856172!4d175.2817617">Timezone</a></h4>
					<p>Who doesn't love going to the arcade? <i>460 Victoria St, Hamilton</i></p>
				</div>
			</div>
			
			<div class="item">	
				<div class="number">5.</div>
				<div class="text">
					<h4><a href="https://www.google.co.nz/maps/place/Lilliputt/@-37.786129,175.2781158,17z/data=!3m1!4b1!4m5!3m4!1s0x6d6d18a455472d37:0x7f7e805f9f8b9084!8m2!3d-37.7861333!4d175.2803045">Lilliputt Minigolf</a></h4>
					<p>It's like golf, but smaller. <i>Centre Place, Hamilton</i></p>
				</div>
			</div>
			
			<div class="item">	
				<div class="number">6.</div>
				<div class="text">
					<h4><a href="https://www.google.co.nz/maps/place/Grey+St+Kitchen+%7C+Hamilton+East+Cafe/@-37.7935681,175.2910415,17z/data=!3m1!4b1!4m5!3m4!1s0x6d6d18c9e150fed7:0x31a05846acee86b9!8m2!3d-37.7935724!4d175.2932302">Grey St Kitchen</a></h4>
					<p>Conveniently located near the Hamilton gardens for a brew and a stroll. <i>355 Grey St, Hamilton</i></p>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section id="registry">	
	<div class="row">	
		<div class="small-12 columns text-center intro-text">
			<h2>Registry</h2>
			<p>We’ve made an online registry if you want to bless us with a gift. If you’re not keen on online shopping, we'd be stoked with a Briscoes or Kmart gift voucher!<br/><br/>If you'd like to send a gift prior to the wedding, our address is:<br/><i>17 Eaglehurst Road, Ellerslie, Auckland 1060</i>.</p>
		</div>

		<?php $loop = new WP_Query( array( 'post_type' => 'products', 'posts_per_page' => -1) ); ?>
		<div class="product-grid">
			<?php while ( $loop->have_posts() ) : $loop->the_post(); 
				$requested = get_field('quantity_requested');
				$bought = get_field('quantity_bought');
				$total_quantity = $requested - $bought;
				$product_ID = get_the_ID();
			?>
			<div class="small-6 medium-3 columns text-center left">
				<div class="product-wrap<?php if($bought >= $requested){ echo ' all-allocated'; } ?>">
					<div class="product-image">	
						<p><?php the_post_thumbnail(); ?></p>
						
						<div class="allocated-overlay">
							<div class="allocated-inner">	
								<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/allocated-icon.svg" width="28"/>
								<p>All Purchased</p>
							</div>
						</div>
					</div>
					<div class="product-details">
						<p><?php the_title(); ?></p>
						<p><?php the_field('price'); ?></p>
					</div>
					<div class="modal-buy-overlay">
						<h3>Did you buy this <?php the_title(); ?>?</h3>
						<p>(Please let us know so we can avoid double ups, thanks!)</p>
						<div class="button-wrap" data-quantity="<?php echo $requested; ?>" data-postid="<?php echo $product_ID; ?>">
							<div class="button light confirm">Yes</div>
							<div class="button light">No</div>
						</div>
					</div>
					<div class="modal-html-content">
						<div class="product-details"></div>
						<div class="image-wrapper">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="content-wrapper">	
							<h3><?php the_title(); ?></h3>
							<p><?php the_field('price'); ?></p>
							<p>Quantity: <span class="quantity-value"><?php echo $total_quantity; ?></span></p>
							<?php if($total_quantity != 0){ ?>
								<a target="_blank" href="<?php the_field('url'); ?>" class="button registry-buy" data-productid="<?php echo $product_ID; ?>">Buy Online</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>
</section>

<div class="product-modal-overlay">
	<div class="product-modal">
		<div class="modal-buy-overlay">
		</div>
		<div class="modal-close modal-close-button">+</div>
		<div class="modal-html-content">
		</div>
	</div>
	<div class="overlay-bg modal-close-button"></div>
</div>

<?php get_footer(); ?>