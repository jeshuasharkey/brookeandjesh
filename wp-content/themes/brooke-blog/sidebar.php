<div class="sidebar">

	<div class="prof-pic">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/profile.jpg" width="140">
	</div>

	<div class="blurb">
		<p><a href="https://www.youtube.com/channel/UCN9CfzNe-YBB1Kiai3LtsoQ">Our Youtube Channel</a></p>
	</div>


	<div class="social-wrap">
		<!-- <a href="pinterest.com" class="social-icon">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pinterest-icon@2x.png" width="15">
		</a> -->
		<a href="http://instagram.com/brookeandjesh" class="social-icon">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram-icon@2x.png" width="16">
		</a>
		<!-- <a href="facebook.com" class="social-icon">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook-icon@2x.png" width="10">
		</a> -->
	</div>
<!--
	<div class="nav">
		<ul>
			<li><a href="">Food</a></li>
			<li><a href="">Family</a></li>
			<li><a href="">Craft</a></li>
			<li><a href="">Places</a></li>
			<li><a href="">Travel</a></li>
		</ul>
	</div>
-->

	<?php if(!is_home()){ ?>
	<!-- <div class="article-single-nav">
		<?php next_post_link('<div class="next-post nav">%link</div>'); ?>
		<?php previous_post_link('<div class="prev-post nav">%link</div>'); ?>
	</div> -->
	<?php } ?>

</div>