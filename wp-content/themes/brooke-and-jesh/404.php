<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */

get_header(); ?>

	<h1>404 Error - Page not found</h1>
	<p>It looks like nothing was found at this location. Maybe try a search?</p>

	<?php get_search_form(); ?>

<?php get_footer(); ?>